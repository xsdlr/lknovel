# encoding: UTF-8 

require 'rubygems' 
require 'bundler/setup'

require 'eeepub'
require 'iconv'

dir = File.join(File.dirname(__FILE__), 'Text')

#p dir

epub = EeePub.make do
  #title     Iconv.iconv("unicode", "utf-8",'閸椾椒绨╅崶鍊燁唶').to_s
  title     '十二国记'
  creator   'caorong'
  publisher 'caorong'
  uid       'BookId'
  identifier 'http://caorong.com', :scheme => 'URL', :id => 'BookId'

  files [File.join(dir, '0.html'), File.join(dir, '1.html')]

  nav [
    {:label => '1. foo', :content => '0.html', :nav => [
      {:label => '1.1 foo-1', :content => '0.html'}
    ]},
    {:label => '1. bar', :content => '1.html'}
  ]
end

epub.save('sample.epub')
