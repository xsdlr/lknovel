# encoding: UTF-8 

require 'rubygems' 
require 'bundler/setup'

require 'eeepub'
require 'iconv'


dir = File.join('十二国记*')

slist = []

Dir.glob(dir ) do |file|
  #p file
  subdir = File.join(file,"Text","*.html")
  sublist = []
  Dir.glob(subdir) do |ff|
    #p ff.methods.sort
    #p ff.split("/")[-1]
    sublist << ff if ff.split('/')[-1] =~ /\d/
  end
  sublist =  sublist.sort_by{ |f| f.scan(/(\d+)\.html/).flatten[0].to_i}
  #p sublist 
  slist.concat(sublist)
end

snav = []

slist.each do |f|
  #p f.split('.')[0]
  #p f
  snav << {:label => f.split('.')[0], :content => f}
end


