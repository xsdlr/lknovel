# encoding: UTF-8 

require 'rubygems' 
require 'bundler/setup'

require 'eeepub'
require 'iconv'


dir = File.join('十二国记*')

slist = []

Dir.glob(dir ) do |file|
  #p file
  subdir = File.join(file,"Text","*.html")
  sublist = []
  Dir.glob(subdir) do |ff|
    #p ff.methods.sort
    #p ff.split("/")[-1]
    sublist << ff if ff.split('/')[-1] =~ /\d/
  end
  sublist =  sublist.sort_by{ |f| f.scan(/(\d+)\.html/).flatten[0].to_i}
  #p sublist 
  slist.concat(sublist)
end

snav = []

slist.each do |f|
  #p f.split('.')[0]
  #p f
  snav << {:label => f.split('.')[0], :content => f}
end

#p slist
#p snav 




ddir = File.join('十二国记  第01卷')

epub = EeePub.make do
  #title     Iconv.iconv("unicode", "utf-8",'閸椾椒绨╅崶鍊燁唶').to_s
  title     '十二国记'
  creator   'caorong'
  publisher 'caorong'
  uid       'BookId'
  identifier 'http://caorong.com', :scheme => 'URL', :id => 'BookId'

  #files [File.join(dir, '0.html'), File.join(dir, '1.html')]
  #files [File.join(ddir, 'Text','0.html'), File.join(ddir, 'Text','1.html')]
  #files = slist

  #nav [
  #{:label => '1. foo', :content => '0.html'},
  #{:label => '1. bar', :content => '1.html'}
  #]

  nav [ 
    #{:label=>"十二国记  第01卷/Text/0", :content=>"十二国记  第01卷/Text/0.html"}, 
    #{:label=>"十二国记  第01卷/Text/1", :content=>"十二国记  第01卷/Text/1.html"}
    
  ]
  


  #nav = snav
end

#p File.join(dir, 'Text',"0.html").class

#epub.save('test.epub')

